package cuarrays

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CUDAFloatArrayBasicTests extends FunSuite {
  val eps = 0.0001

  test("sum for small array") {
    val floatArray = new CUDAFloatArray(Array[Float](1, 2, 3))
    assert(floatArray.sum === 1 + 2 + 3)
    floatArray.delete
  }

  test("min for small array") {
    val floatArray = new CUDAFloatArray(Array[Float](1, 2, 3))
    assert(floatArray.max === 3)
    floatArray.delete
  }

  test("max for small array") {
    val floatArray = new CUDAFloatArray(Array[Float](1, 2, 3))
    assert(floatArray.min === 1)
    val reversed = floatArray.reversed
    assert(reversed.min === floatArray.min)
    floatArray.delete
    reversed.delete
  }

  test("getElement on simple examples") {
    val array = createCudaArrayOneToN(500)
    assert(array.getElement(0) === 1f)
    assert(array.getElement(5) === 6f)
    assert(array.getElement(50) === 51f)
    assert(array.getElement(499) === 500f)
    intercept[CuArraysException] {
      array.getElement(500)
    }
    intercept[CuArraysException] {
      array.getElement(-1)
    }
    array.delete
  }

  test("getHostSubArray on simple examples") {
    val array = createCudaArrayOneToN(500)
    assert(array.getHostSubArray(0, 10) === (1 until 11).map(_.toFloat).toArray)
    assert(array.getHostSubArray(20, 10) === (21 until 31).map(_.toFloat).toArray)
    intercept[CuArraysException] {
      array.getHostSubArray(0, -5)
    }
    intercept[CuArraysException] {
      array.getHostSubArray(-5, 10)
    }
    array.delete
  }

  test("getSubArray on simple examples") {
    val array = createCudaArrayOneToN(500)
    assert(array.getSubArray(0, 10).toHostArray() === (1 until 11).map(_.toFloat).toArray)
    assert(array.getSubArray(20, 10).toHostArray() === (21 until 31).map(_.toFloat).toArray)
    intercept[CuArraysException] {
      array.getSubArray(0, -5)
    }
    intercept[CuArraysException] {
      array.getSubArray(-5, 10)
    }
    array.delete
  }

  test("reverse simple arrays") {
    testReverse(500)
    testReverse(1024)
    testReverse(4 * 1024)
    testReverse(4 * 1024 + 2)
    testReverse(8 * 1024)
    testReverse(8 * 1024 - 3)
  }
  
  test("element wise adding") {
    val cuArray = new CUDAFloatArray(Array[Float](1, 2, 3))
    assert((cuArray + cuArray).toHostArray().deep == Array[Float](2, 4, 6).deep)
    cuArray.delete
  }

  test("element wise subtracting") {
    val cuArray = new CUDAFloatArray(Array[Float](1, 2, 3))
    assert((cuArray - cuArray).toHostArray().deep == Array[Float](0, 0, 0).deep)
    cuArray.delete
  }

  test("element wise multiply") {
    val cuArray = new CUDAFloatArray(Array[Float](1, 2, 3))
    assert((cuArray * cuArray).toHostArray().deep == Array[Float](1, 4, 9).deep)
    cuArray.delete
  }

  test("element wise division") {
    val cuArray = new CUDAFloatArray(Array[Float](1, 2, 3))
    assert((cuArray / cuArray).toHostArray().deep == Array[Float](1, 1, 1).deep)
    cuArray.delete
  }

  def createCudaArrayOneToN(n: Int): CUDAFloatArray =
    new CUDAFloatArray(createArrayOneToN(n))

  def createArrayOneToN(n: Int): Array[Float] =
    (1 until n + 1).map(_.toFloat).toArray

  def testReverse(length: Int) = {
    val array = createCudaArrayOneToN(length)
    val revArray = array.reversed
    assert(array.length === revArray.length)
    val h_rev = revArray.getHostSubArray(0, length)

    var idx = 0
    var value = length.toFloat
    while (idx < length) {
      assert(h_rev(idx) === value, "for index: " + idx)
      idx += 1
      value -= 1.0F
    }
    array.delete
    revArray.delete
  }
}