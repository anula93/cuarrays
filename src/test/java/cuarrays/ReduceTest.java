package cuarrays;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.CUdeviceptr;
import jcuda.driver.JCudaDriver;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Tests method
 * {@link CUDAAlgorithms#reduce(jcuda.driver.CUdeviceptr, int, cuarrays.CUDAAlgorithms.ReduceOp)}
 * ;
 * 
 * @author Anna Szybalska <anula.szy@gmail.com>
 *
 */
public class ReduceTest {
  @Rule
  public ExpectedException exception = ExpectedException.none();

  @Test
  public void smallOnesArrays() throws CuArraysException {
    testOnesArray(10);
    testOnesArray(63);
    testOnesArray(64);
    testOnesArray(65);
    testOnesArray(100);
    testOnesArray(1000);
  }

  @Test
  public void smallOneToNArrays() throws CuArraysException {
    testOneToNArray(10);
    testOneToNArray(63);
    testOneToNArray(64);
    testOneToNArray(65);
    testOneToNArray(100);
    testOneToNArray(1000);
  }

  @Test
  public void mediumOnesArrays() throws CuArraysException {
    testOnesArray(2 * 1024);
    testOnesArray(4 * 1024 - 1);
    testOnesArray(3 * 1024 + 599);
    testOnesArray(1234567);
    testOnesArray(1000000);
  }
  
  @Test
  public void bigOnesArray() throws CuArraysException {
    testOnesArray(63000000);
    testOnesArray(126000000);
  }

  private void testOnesArray(int length) throws CuArraysException {
    float epsi = 0.0001F;
    float[] ones = generateOnesArray(length);
    CUDAContextManager.getContext();
    CUdeviceptr d_ones = null;
    try {
      d_ones = copyArrayToDevice(ones);
      assertEquals("onesArray, ADD, length " + length, (float) length,
          CUDAAlgorithms.reduce(d_ones, length, CUDAAlgorithms.ReduceOp.ADD),
          epsi);
      assertEquals("onesArray, MIN, length " + length, 1.0,
          CUDAAlgorithms.reduce(d_ones, length, CUDAAlgorithms.ReduceOp.MIN),
          epsi);
      assertEquals("onesArray, MAX, length " + length, 1.0,
          CUDAAlgorithms.reduce(d_ones, length, CUDAAlgorithms.ReduceOp.MAX),
          epsi);
    } finally {
      try {
        if (d_ones != null) {
          JCudaDriver.cuMemFree(d_ones);
        }
      } catch (Exception e) {
        // Ignoring exception with freeing memory. It means that memory was
        // already freed or never allocated.
      }
      CUDAContextManager.releaseContext();
    }
  }

  private void testOneToNArray(int length) throws CuArraysException {
    float[] array = generateOneToNArray(length);
    CUDAContextManager.getContext();
    CUdeviceptr d_array = null;
    try {
      d_array = copyArrayToDevice(array);
      assertEquals("oneToNArray, ADD, length " + length, length * (length + 1)
          / 2, (int) CUDAAlgorithms.reduce(d_array, length,
          CUDAAlgorithms.ReduceOp.ADD));
      assertEquals("oneToNArray, MIN, length " + length, 1,
          (int) CUDAAlgorithms.reduce(d_array, length,
              CUDAAlgorithms.ReduceOp.MIN));
      assertEquals("oneToNArray, MAX, length " + length, length,
          (int) CUDAAlgorithms.reduce(d_array, length,
              CUDAAlgorithms.ReduceOp.MAX));
    } finally {
      try {
        if (d_array != null) {
          JCudaDriver.cuMemFree(d_array);
        }
      } catch (Exception e) {
        // Ignoring exception with freeing memory. It means that memory was
        // already freed or never allocated.
      }
      CUDAContextManager.releaseContext();
    }
  }

  private float[] generateOnesArray(int length) {
    float array[] = new float[length];
    Arrays.fill(array, 1);
    return array;
  }

  private float[] generateOneToNArray(int length) {
    float array[] = new float[length];
    for (int i = 0; i < length; i++) {
      array[i] = i + 1;
    }
    return array;
  }

  private CUdeviceptr copyArrayToDevice(float[] h_array) {
    CUdeviceptr d_array = new CUdeviceptr();
    long bytesNum = h_array.length * Sizeof.FLOAT;
    JCudaDriver.cuMemAlloc(d_array, bytesNum);
    JCudaDriver.cuMemcpyHtoD(d_array, Pointer.to(h_array), bytesNum);
    return d_array;
  }
}
