package cuarrays;

import java.util.logging.Logger;

import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.CUdeviceptr;
import jcuda.driver.JCudaDriver;

class BckCUDAFloatArray {
  private static final Logger logger = Logger.getLogger(BckCUDAFloatArray.class
      .getName());

  private CUdeviceptr d_array = new CUdeviceptr();
  private int length;
  private boolean initialized = false;

  BckCUDAFloatArray(float[] elements) throws CuArraysException {
    length = elements.length;
    try {
      CUDAContextManager.getContext();
      JCudaDriver.cuMemAlloc(d_array, length * Sizeof.FLOAT);
      JCudaDriver.cuMemcpyHtoD(d_array, Pointer.to(elements), length
          * Sizeof.FLOAT);
    } catch (jcuda.CudaException e) {
      throw new CuArraysException("Problem with copying data to device memory",
          e);
    } finally {
      CUDAContextManager.releaseContext();
      initialized = true;
    }
  }

  /**
   * Creates object directly from pointer to device memory. It takes ownership
   * of passed pointer.
   * 
   * @param d_array
   *          Pointer to array in device memory. Constructor takes ownership
   *          over this pointer.
   */
  private BckCUDAFloatArray(CUdeviceptr d_array, int length) {
    this.d_array = d_array;
    this.length = length;
    initialized = true;
  }

  BckCUDAFloatArray getSubArray(int startIdx, int length) {
    if (startIdx + length > this.length || length < 1 || startIdx < 0) {
      throw new CuArraysException(
          "Invalid arguments: requested subarray doesn't exists.");
    }
    try {
      CUDAContextManager.getContext();
      CUdeviceptr d_subArray = new CUdeviceptr();
      JCudaDriver.cuMemAlloc(d_subArray, length * Sizeof.FLOAT);
      JCudaDriver.cuMemcpyDtoD(d_subArray,
          d_array.withByteOffset(startIdx * Sizeof.FLOAT), length
              * Sizeof.FLOAT);
      return new BckCUDAFloatArray(d_subArray, length);
    } catch (jcuda.CudaException e) {
      throw new CuArraysException("Error while extracting subarray", e);
    } finally {
      CUDAContextManager.releaseContext();
    }
  }

  /**
   * Returns sub array of given length starting in specified index. The array
   * resides in host memory. Getting the array requires copying it from device
   * memory, so using it often is probably not very good idea.
   * 
   * @param startIdx
   *          Index where requested sub array starts.
   * @param length
   *          Length of requested sub array.
   * @return Sub array in host memory.
   */
  float[] getHostSubArray(int startIdx, int length) {
    if (startIdx + length > this.length || length < 1 || startIdx < 0) {
      throw new CuArraysException(
          "Invalid arguments: requested subarray doesn't exists.");
    }
    float slice[] = new float[length];
    try {
      CUDAContextManager.getContext();
      JCudaDriver.cuMemcpyDtoH(Pointer.to(slice),
          d_array.withByteOffset(Sizeof.FLOAT * startIdx), length
              * Sizeof.FLOAT);
    } catch (jcuda.CudaException e) {
      throw new CuArraysException("Error while extracting subarray", e);
    } finally {
      CUDAContextManager.releaseContext();
    }
    return slice;
  }

  /**
   * Returns element of array in given index in host memory. It is helper
   * function that underneath just calls {@link #getHostSubArray(int, int)} with
   * array of size one, so overhead of coping element from device to host memory
   * exists here as well.
   * 
   * @param index
   *          Index of requested element.
   * @return Requested element of the array.
   */
  float getElement(int index) {
    float[] subarray = getHostSubArray(index, 1);
    return subarray[0];
  }

  int getLength() {
    return length;
  }

  /**
   * Performs element wise operation on two FloatArrays returning new as result.
   * 
   * @param other
   * @param operation
   * @return
   */
  BckCUDAFloatArray elementWise(BckCUDAFloatArray other,
      CUDAAlgorithms.ElemWiseOp operation) {
    checkInitialized();
    other.checkInitialized();
    if (length != other.length) {
      throw new CuArraysException(
          "Dimensions are not compatibile for element wise operation! "
              + length + " != " + other.length);
    }
    BckCUDAFloatArray result;
    try {
      CUDAContextManager.getContext();
      CUdeviceptr d_result = CUDAAlgorithms.elementWise(d_array, other.d_array,
          length, operation);
      result = new BckCUDAFloatArray(d_result, length);
    } catch (jcuda.CudaException e) {
      throw new CuArraysException("Error while reversing", e);
    } finally {
      CUDAContextManager.releaseContext();
    }
    return result;
  }

  BckCUDAFloatArray reverse() {
    checkInitialized();
    BckCUDAFloatArray reversed;
    try {
      CUDAContextManager.getContext();
      CUdeviceptr d_reversed = CUDAAlgorithms.reverse(d_array, length);
      reversed = new BckCUDAFloatArray(d_reversed, length);
    } catch (jcuda.CudaException e) {
      throw new CuArraysException("Error while reversing", e);
    } finally {
      CUDAContextManager.releaseContext();
    }
    return reversed;
  }

  float reduce(CUDAAlgorithms.ReduceOp reduceOp) {
    checkInitialized();
    float reduced;
    try {
      CUDAContextManager.getContext();
      reduced = CUDAAlgorithms.reduce(d_array, length, reduceOp);
    } catch (jcuda.CudaException e) {
      throw new CuArraysException("Error while running reduce", e);
    } finally {
      CUDAContextManager.releaseContext();
    }
    return reduced;
  }

  /**
   * Should be called when object is no longer used. It releases resources
   * acquired in constructor.
   */
  void delete() {
    try {
      CUDAContextManager.getContext();
      JCudaDriver.cuMemFree(d_array);
    } finally {
      CUDAContextManager.releaseContext();
      initialized = false;
    }
  }

  private void checkInitialized() throws CuArraysException {
    if (!initialized) {
      throw new CuArraysException(
          "Object is not properly initialized! Either constructor "
              + "had thrown an exception or method delete was already "
              + "called on this object!");
    }
  }

  @Override
  protected void finalize() throws Throwable {
    try {
      if (initialized) {
        delete();
        throw new CuArraysException(
            "Method delete() was not called on this object (" + this
                + ") before garbage collection!");
      }
    } finally {
      super.finalize();
    }
  }
}
