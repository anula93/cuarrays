package cuarrays;

import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import jcuda.driver.CUcontext;
import jcuda.driver.CUdevice;
import jcuda.driver.JCudaDriver;

/**
 * Manages one context, possibly among many threads, but only one thread can use
 * the context at one time.
 * 
 * @author Anna Szybalska <anula.szy@gmail.com>
 *
 */
public class CUDAContextManager {
  private static final Logger logger = Logger
      .getLogger(CUDAContextManager.class.getName());

  private static int deviceNumber = 0;
  private static CUcontext cuContext = null;

  private static ReentrantLock contextLock = new ReentrantLock();

  private CUDAContextManager() {
  }

  /**
   * Should be called by each function that wants to use device at its very
   * beginning.
   */
  public static void getContext() {
    contextLock.lock();
    try {
      createContextIfNonExistent();
      JCudaDriver.cuCtxPushCurrent(cuContext);
    } catch (Exception e) {
      contextLock.unlock();
      throw e;
    }
    logger
        .finest("Thread " + Thread.currentThread().getId() + " aquires context");
  }

  /**
   * Should be called after each function that uses CUDA after finishing using
   * it.
   */
  public static void releaseContext() {
    if (!contextLock.isHeldByCurrentThread()) {
      logger.warning("releaseContext was called by thread "
          + "that doesn't have contextLock");
    }
    try {
      JCudaDriver.cuCtxPopCurrent(cuContext);
    } finally {
      contextLock.unlock();
    }
    logger.finest("Thread " + Thread.currentThread().getId()
        + " releases context");
  }

  public static boolean isContextCreated() {
    return (cuContext == null);
  }

  public synchronized static void destroyCurrentContext() {
    if (cuContext != null) {
      JCudaDriver.cuCtxDestroy(cuContext);
      cuContext = null;
    }
  }

  /**
   * If there is already existing context you need to call {@link
   * destroyCurrentContext()} in order for new context to be created on device
   * specified here.
   * 
   * @param number
   *          Device number on which future context will be created.
   */
  public static void setDeviceNumber(int number) {
    deviceNumber = number;
  }

  private static void createContextIfNonExistent() {
    if (cuContext == null) {
      JCudaDriver.setExceptionsEnabled(true);
      JCudaDriver.cuInit(0);
      cuContext = new CUcontext();
      CUdevice device = new CUdevice();
      JCudaDriver.cuDeviceGet(device, deviceNumber);
      JCudaDriver.cuCtxCreate(cuContext, deviceNumber, device);
      JCudaDriver.cuCtxPopCurrent(cuContext);
    }
  }

}
