package cuarrays;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.CUdeviceptr;
import jcuda.driver.CUfunction;
import jcuda.driver.CUmodule;
import jcuda.driver.JCudaDriver;

/**
 * Various algorithms to run on GPU. If not otherwise specified they require
 * CUDA context to be already initialized.
 * 
 * @author Anna Szybalska <anula.szy@gmail.com>
 */
class CUDAAlgorithms {
  private static final Logger logger = Logger.getLogger(CUDAAlgorithms.class
      .getName());
  private static boolean DEBUG_MODE = true;

  private static final String REDUCE_KERNEL = "/kernels/reduce";
  private static final String REVERSE_KERNEL = "/kernels/reverse";
  private static final String ELEM_WISE_KERNEL = "/kernels/elem_wise";

  private static String ptxKernelsPath = System.getProperty("java.io.tmpdir")
      + "/cuarrays/kernels/";

  // ======== CUDA specific constants

  private static int MAX_THREADS_PER_BLOCK = 1024;

  // ======== reduce

  enum ReduceOp {
    ADD(0, 0, new EvalFunction() {
      public float eval(float arg1, float arg2) {
        return arg1 + arg2;
      }
    }), MIN(1, Float.MAX_VALUE, new EvalFunction() {
      public float eval(float arg1, float arg2) {
        return Math.min(arg1, arg2);
      }
    }), MAX(2, -Float.MAX_VALUE, new EvalFunction() {
      public float eval(float arg1, float arg2) {
        return Math.max(arg1, arg2);
      }
    });

    private int value;
    private EvalFunction evalFunction;
    private float identity;

    private ReduceOp(int value, float identity, EvalFunction evalFunction) {
      this.value = value;
      this.evalFunction = evalFunction;
    }

    int getValue() {
      return value;
    }

    float eval(float arg1, float arg2) {
      return evalFunction.eval(arg1, arg2);
    }

    float getIdentity() {
      return identity;
    }

    interface EvalFunction {
      float eval(float arg1, float arg2);
    }
  }

  /**
   * Reduces array using specified operation.
   * 
   * @param d_array
   *          Device pointer to array to reduce.
   * @param length
   *          Length of array to reduce.
   * @param reduceOp
   *          One of allowed operations to use for reducing.
   * @return Result of reduction on the array.
   * @throws CuArraysException
   *           when any CUDA exception is raised or kernel cannot be compiled.
   */
  static float reduce(CUdeviceptr d_array, int length, ReduceOp reduceOp)
      throws CuArraysException {

    int blockLength = 2 * MAX_THREADS_PER_BLOCK;
    float result;
    CUdeviceptr d_inArray = null;
    CUdeviceptr d_outArray = null;
    CUmodule module = null;
    try {
      if (length < blockLength) {
        return hostReduce(d_array, length, reduceOp);
      }

      module = loadModule(REDUCE_KERNEL);
      CUfunction reduceFunction = new CUfunction();
      JCudaDriver.cuModuleGetFunction(reduceFunction, module, "Reduce");

      // Greatest power of two that is smaller or equal to length.
      int inLength = nextPowerOfTwo(length);
      if (inLength > length) {
        inLength /= 2;
      }

      d_inArray = createFloatDeviceArray(length);
      JCudaDriver.cuMemcpyDtoD(d_inArray, d_array, length * Sizeof.FLOAT);
      d_outArray = createFloatDeviceArray(inLength / blockLength);

      int realLength = length;

      while (inLength >= blockLength) {
        int outLength = inLength / blockLength;
        gpuReduce(d_inArray, realLength, reduceFunction, MAX_THREADS_PER_BLOCK,
            outLength, reduceOp, d_outArray);
        inLength = outLength;
        realLength = outLength;
        CUdeviceptr tmp = d_outArray;
        d_outArray = d_inArray;
        d_inArray = tmp;
      }

      result = hostReduce(d_inArray, inLength, reduceOp);

      JCudaDriver.cuMemFree(d_inArray);
      JCudaDriver.cuMemFree(d_outArray);
      JCudaDriver.cuModuleUnload(module);
    } catch (jcuda.CudaException | IOException e) {
      logger.severe("Reduction failed due to: " + e.toString());
      throw new CuArraysException("Running reduce failed.", e);
    }
    return result;
  }

  /**
   * Runs reduce on specified input array, using specified operator, and number
   * of blocks and threads.
   * 
   * @param d_array
   *          An array to be reduced in device memory.
   * @param length
   *          Number of elements in the array.
   * @param reduceFunction
   *          CUDA reduce function.
   * @param threadsNum
   *          Number of threads per block to run.
   * @param blocksNum
   *          Number of blocks to run.
   * @param operation
   *          Operation to use during reduce.
   * @param d_output
   *          A device memory where output will be written.
   */
  private static void gpuReduce(CUdeviceptr d_array, int length,
      CUfunction reduceFunction, int threadsNum, int blocksNum,
      ReduceOp operation, CUdeviceptr d_output) {
    Pointer kernelParameters = Pointer.to(Pointer.to(d_array),
        Pointer.to(new int[] { length }), Pointer.to(d_output),
        Pointer.to(new int[] { operation.getValue() }));

    int sharedMemSize = threadsNum * Sizeof.FLOAT;
    JCudaDriver.cuLaunchKernel(reduceFunction, blocksNum, 1, 1, threadsNum, 1,
        1, sharedMemSize, null, kernelParameters, null);
    JCudaDriver.cuCtxSynchronize();
  }

  private static float hostReduce(CUdeviceptr d_array, int length,
      ReduceOp operator) {
    float h_array[] = new float[length];
    JCudaDriver.cuMemcpyDtoH(Pointer.to(h_array), d_array, length
        * Sizeof.FLOAT);

    if (length == 1) {
      return h_array[0];
    }

    float reduced = operator.eval(h_array[0], h_array[1]);
    for (int i = 2; i < length; i++) {
      reduced = operator.eval(reduced, h_array[i]);
    }
    return reduced;
  }

  // ======== revert

  /**
   * Reverses given array and returns pointer to new, reversed array. Assumes
   * that context was already initialized.
   * 
   * 
   * @param d_array
   *          Array in device memory to be reversed
   * @param length
   *          Length of d_array
   * @return Pointer to reversed array in device memory
   */
  static CUdeviceptr reverse(CUdeviceptr d_array, int length) {
    CUdeviceptr d_reversed = new CUdeviceptr();
    try {
      JCudaDriver.cuMemAlloc(d_reversed, length * Sizeof.FLOAT);

      CUmodule module = loadModule(REVERSE_KERNEL);
      CUfunction reverseFunction = new CUfunction();
      JCudaDriver.cuModuleGetFunction(reverseFunction, module, "Reverse");

      // Manually chosen
      int elemsPerThread = 4;
      int threadsNum = MAX_THREADS_PER_BLOCK;
      int blockLength = elemsPerThread * threadsNum;
      int blocksNum = (int) Math.ceil((double) length / blockLength);

      gpuReverse(d_array, length, reverseFunction, elemsPerThread, threadsNum,
          blocksNum, d_reversed);

    } catch (jcuda.CudaException | IOException e) {
      logger.severe("Reverse failed due to: " + e.toString());
      throw new CuArraysException("Reverse failed.", e);
    }
    return d_reversed;
  }

  private static void gpuReverse(CUdeviceptr d_in, int length,
      CUfunction reverseFunction, int elemsPerThread, int threadsNum,
      int blocksNum, CUdeviceptr d_out) {
    Pointer kernelParameters = Pointer.to(Pointer.to(d_in),
        Pointer.to(new int[] { length }),
        Pointer.to(new int[] { elemsPerThread }), Pointer.to(d_out));

    int sharedMemSize = elemsPerThread * threadsNum * Sizeof.FLOAT;
    JCudaDriver.cuLaunchKernel(reverseFunction, blocksNum, 1, 1, threadsNum, 1,
        1, sharedMemSize, null, kernelParameters, null);
    JCudaDriver.cuCtxSynchronize();
  }

  // ======== elemWise

  enum ElemWiseOp {
    ADD(0), SUB(1), MUL(2), DIV(3);

    private int value;

    private ElemWiseOp(int value) {
      this.value = value;
    }

    int getValue() {
      return value;
    }
  }

  static CUdeviceptr elementWise(CUdeviceptr d_arg1, CUdeviceptr d_arg2,
      int length, ElemWiseOp op) {
    CUdeviceptr d_result = new CUdeviceptr();
    try {
      JCudaDriver.cuMemAlloc(d_result, length * Sizeof.FLOAT);

      CUmodule module = loadModule(ELEM_WISE_KERNEL);
      CUfunction elemWiseFunction = new CUfunction();
      JCudaDriver.cuModuleGetFunction(elemWiseFunction, module, "ElemWise");

      // Manually chosen
      int elemsPerThread = 4;
      int threadsNum = MAX_THREADS_PER_BLOCK;
      int blockLength = elemsPerThread * threadsNum;
      int blocksNum = (int) Math.ceil((double) length / blockLength);

      Pointer kernelParameters = Pointer.to(Pointer.to(d_arg1),
          Pointer.to(d_arg2), Pointer.to(new int[] { length }),
          Pointer.to(new int[] { op.getValue() }),
          Pointer.to(new int[] { elemsPerThread }), Pointer.to(d_result));

      JCudaDriver.cuLaunchKernel(elemWiseFunction, blocksNum, 1, 1, threadsNum,
          1, 1, 0, null, kernelParameters, null);
      JCudaDriver.cuCtxSynchronize();

    } catch (jcuda.CudaException | IOException e) {
      logger.severe("ElemWise failed due to: " + e.toString());
      throw new CuArraysException("ElemWise failed.", e);
    }
    return d_result;
  }

  // ======== helpers

  /**
   * Returns smallest power of two greater or equal to x.
   * 
   * @param x
   *          Should be grater than 0.
   * @return Smallest power of two greater or equal to x.
   */
  private static int nextPowerOfTwo(int x) {
    if (x <= 1)
      return x;
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return x + 1;
  }

  private static CUdeviceptr createFloatDeviceArray(int length) {
    CUdeviceptr devPtr = new CUdeviceptr();
    JCudaDriver.cuMemAlloc(devPtr, length * Sizeof.FLOAT);
    return devPtr;
  }

  private static CUmodule loadModule(String moduleFileName) throws IOException {
    File resCuFile = new File(CUDAAlgorithms.class.getResource(
        moduleFileName + ".cu").getFile());
    String cuFileName = ptxKernelsPath + resCuFile.getName();
    saveStreamAsFile(
        CUDAAlgorithms.class.getResourceAsStream(moduleFileName + ".cu"),
        cuFileName);
    String  ptxFileName = preparePtxFile(cuFileName);

    CUmodule module = new CUmodule();
    JCudaDriver.cuModuleLoad(module, ptxFileName);

    return module;
  }

  /**
   * <p>
   * NOT SAFE FOR USER INPUT! Don't pass file name as an argument unless you are
   * sure it is actual name of file containing kernel!.
   * </p>
   * <p>
   * Compiles given kernel file to PTX file, unless PTX file of matching name
   * already exists. Resulting PTX file has the same name with extension changed
   * (or added if original file has no extension) to ".ptx".
   * </p>
   * <p>
   * Compilation is done using NVCC. Returns name of PTX file.
   * </p>
   * 
   * @param cuFileName
   *          The name of the .CU file
   * 
   * @return The name of the PTX file
   * 
   * @throws IOException
   *           If an I/O error occurs
   */
  private static String preparePtxFile(String cuFileName) throws IOException {
    int endIndex = cuFileName.lastIndexOf('.');
    if (endIndex == -1) {
      endIndex = cuFileName.length();
    }
    File cuFile = new File(cuFileName);
    if (!cuFile.exists()) {
      throw new IOException("Kernel file not found: " + cuFileName);
    }

    String ptxFileName = cuFileName.substring(0, endIndex) + ".ptx";
    File ptxFile = new File(ptxFileName);
    if (!DEBUG_MODE && ptxFile.exists()) {
      return ptxFileName;
    }

    File tempPtxFile = new File(ptxKernelsPath + ptxFile.getName());

    if (!tempPtxFile.exists()) {
      tempPtxFile.getParentFile().mkdirs();
      tempPtxFile.createNewFile();
    }

    String modelString = "-m" + System.getProperty("sun.arch.data.model");
    StringBuilder commandBuilder = new StringBuilder("nvcc ");
    commandBuilder.append(modelString + " ");
    commandBuilder.append("-ptx " + cuFile.getPath() + " ");
    commandBuilder.append("-o " + tempPtxFile.getPath() + " ");
    String command = commandBuilder.toString();

    logger.finest("Executing command: " + command);
    Process process = Runtime.getRuntime().exec(command);

    String errorMessage = new String(toByteArray(process.getErrorStream()));
    String outputMessage = new String(toByteArray(process.getInputStream()));
    int exitValue = 0;
    try {
      exitValue = process.waitFor();
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      throw new IOException("Interrupted while waiting for nvcc output", e);
    }

    if (exitValue != 0) {
      logger.severe("nvcc process exitValue: " + exitValue);
      logger.severe("errorMessage: " + errorMessage);
      logger.severe("outputMessage: " + outputMessage);
      throw new IOException("Could not create .ptx file: " + errorMessage);
    }

    logger.finest(String.format("Compiled %s to PTX", cuFile.getPath()));
    return tempPtxFile.getPath();
  }

  /**
   * Fully reads the given InputStream and returns it as a byte array
   * 
   * @param inputStream
   *          The input stream to read
   * @return The byte array containing the data from the input stream
   * @throws IOException
   *           If an I/O error occurs
   */
  private static byte[] toByteArray(InputStream inputStream) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    byte buffer[] = new byte[8192];
    while (true) {
      int read = inputStream.read(buffer);
      if (read == -1) {
        break;
      }
      baos.write(buffer, 0, read);
    }
    return baos.toByteArray();
  }

  private static void saveStreamAsFile(InputStream inStream, String destName)
      throws IOException {
    FileOutputStream fos = null;
    try {
      fos = new FileOutputStream(destName);
      byte[] buf = new byte[2048];
      int r = inStream.read(buf);
      while (r != -1) {
        fos.write(buf, 0, r);
        r = inStream.read(buf);
      }
    } finally {
      if (fos != null) {
        fos.close();
      }
    }
  }
}
