package cuarrays;

public class CuArraysException extends RuntimeException {
  public CuArraysException(String message) {
    super(message);
  }
  
  public CuArraysException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
