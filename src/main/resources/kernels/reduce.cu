#include <cstdio>
#include <cstdlib>

extern "C" {

__device__ float add_func(float x, float y) {
	return x + y;
}

__device__ float min_func(float x, float y) {
	return x > y ? y : x;
}

__device__ float max_func(float x, float y) {
	return x > y ? x : y;
}

typedef float (*op_func)(float, float);

__device__ op_func op_table[3] = { add_func, min_func, max_func };

/**
 * Runs one level of binary tree reduce.
 * blockDim.x should be power of two.
 * size of s_data should equals blockDim.x
 *
 * @param g_array Array to be reduced.
 * @param length Length of g_array
 * @param g_result Array for partial results of reduce. Each block of
 * (2*size_of_s_data) gets reduced into one element of the array. Length of
 * the array should therefore equals length/(2*size_of_s_data).
 */
__global__ void Reduce(float* g_array, int length, float* g_result, int op) {
	// this should have the size of block width
	extern __shared__ float s_data[];

	int s_idx = threadIdx.x;
	int g_idx = blockIdx.x * blockDim.x * 2 + s_idx;
	int grid_threads = 2 * blockDim.x * gridDim.x;

	float my_sum = op_table[op](g_array[g_idx], g_array[g_idx + blockDim.x]);
	for (int idx = g_idx + grid_threads; idx < length; idx += grid_threads) {
		my_sum = op_table[op](my_sum, g_array[idx]);
		if (idx + blockDim.x < length) {
			my_sum = op_table[op](my_sum, g_array[idx + blockDim.x]);
		}
	}

	s_data[s_idx] = my_sum;

	__syncthreads();

	for (int OFFSET = blockDim.x >> 1; OFFSET > 0; OFFSET >>= 1) {
		if (s_idx < OFFSET) {
			float left = s_data[s_idx];
			float right = s_data[s_idx + OFFSET];
			s_data[s_idx] = op_table[op](left, right);
		}
		__syncthreads();
	}
	if (s_idx == 0) {
		g_result[blockIdx.x] = s_data[0];
	}
}

}
