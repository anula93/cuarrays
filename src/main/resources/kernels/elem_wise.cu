#include <cstdio>

extern "C" {

__device__ float add_func(float x, float y) {
	return x + y;
}

__device__ float sub_func(float x, float y) {
	return x - y;
}

__device__ float mul_func(float x, float y) {
	return x * y;
}

__device__ float div_func(float x, float y) {
	return x / y;
}

typedef float (*op_func)(float, float);

__device__ op_func op_table[4] = { add_func, sub_func, mul_func, div_func };

__global__ void ElemWise(float* g_arg1, float* g_arg2, int length, int op,
		int elems_per_thread, float* g_out) {
	int block_length = elems_per_thread * blockDim.x;
	int block_start = block_length * blockIdx.x;
	int thidx = threadIdx.x;
	int g_idx = block_start + thidx;


	for (int idx = g_idx; idx < length; idx += blockDim.x) {
		g_out[idx] = op_table[op](g_arg1[idx], g_arg2[idx]);
	}
}

}
