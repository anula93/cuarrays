#include <cstdio>

extern "C" {

__global__ void Reverse(float* g_in, int length, int elems_per_thread,
		float* g_out) {
	extern __shared__ float s_data[];

	int block_length = elems_per_thread * blockDim.x;
	int in_offset = block_length * blockIdx.x;
	int tidx = threadIdx.x;

	int local_offset = in_offset + block_length - length;
	if (local_offset < 0) {
		local_offset = 0;
	}

	int out_offset = length - in_offset;

	for (int idx = tidx; idx < block_length && in_offset + idx < length; idx +=
			blockDim.x) {
		s_data[block_length - idx - 1] = g_in[in_offset + idx];
	}

	__syncthreads();

	for (int g_idx = out_offset - block_length + local_offset + tidx, l_idx =
			local_offset + tidx; l_idx < block_length;
			l_idx += blockDim.x, g_idx += blockDim.x) {
		g_out[g_idx] = s_data[l_idx];
	}
}

}
