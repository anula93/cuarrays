package cuarrays

/**
 * Class representing immutable array.
 * After creation array is stored in device memory, so operations on big
 * arrays are faster than on normal Scala array.
 */
class CUDAFloatArray private(bckFloatArray: BckCUDAFloatArray) {
  def this(elements: Array[Float]) = this(new BckCUDAFloatArray(elements))
  private val bckFloatArray_ = bckFloatArray;

  /**
   * Method that has to be called after finishing usage of object.
   * It frees device resources required to store array.
   */
  def delete = bckFloatArray_.delete()
  
  /** Returns length of the array
   */
  val length = bckFloatArray_.getLength()

  /** Returns sum of values in the array
   */
  lazy val sum = bckFloatArray_.reduce(CUDAAlgorithms.ReduceOp.ADD)

  /** Returns max of values in the array
   */
  lazy val max = bckFloatArray_.reduce(CUDAAlgorithms.ReduceOp.MAX)

  /** Returns min of values in the array
   */
  lazy val min = bckFloatArray_.reduce(CUDAAlgorithms.ReduceOp.MIN)
  
  /** Element wise addition
   */
  def +(other: CUDAFloatArray) =
    new CUDAFloatArray(
        bckFloatArray_.elementWise(other.bckFloatArray_, CUDAAlgorithms.ElemWiseOp.ADD))

  /** Element wise subtraction
   */
  def -(other: CUDAFloatArray) =
    new CUDAFloatArray(
        bckFloatArray_.elementWise(other.bckFloatArray_, CUDAAlgorithms.ElemWiseOp.SUB))

  /** Element wise multiplication
   */
  def *(other: CUDAFloatArray) =
    new CUDAFloatArray(
        bckFloatArray_.elementWise(other.bckFloatArray_, CUDAAlgorithms.ElemWiseOp.MUL))

  /** Element wise division
   */
  def /(other: CUDAFloatArray) =
    new CUDAFloatArray(
        bckFloatArray_.elementWise(other.bckFloatArray_, CUDAAlgorithms.ElemWiseOp.DIV))
  
  /** Returns new CUDAFloatArray that is requested subarray of the original.
   */
  def getSubArray(startIdx: Int, length: Int) = 
    new CUDAFloatArray(bckFloatArray_.getSubArray(startIdx, length))

  /** Returns requested element in host memory.
   */
  def getElement(index: Int) = bckFloatArray_.getElement(index)

  /** Returns requested subarray in host memory.
   */
  def getHostSubArray(startIdx: Int, length: Int) =
    bckFloatArray_.getHostSubArray(startIdx, length)

  /** Converts object to array in host memory
   */
  def toHostArray() = bckFloatArray_.getHostSubArray(0, this.length)
  
  /** Returns object being reversed array. Object needs to be deleted after usage.
   */
  def reversed = new CUDAFloatArray(bckFloatArray_.reverse())
}