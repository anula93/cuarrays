# README #

Library implementing very simple array for Scala language with backend in CUDA, so all operations on such array are done in CUDA.

### How do run tests? ###

* Clone repository
* Install Scala and sbt
* Install CUDA toolkit
* Set PATH and LD_LIBRARY_PATH correctly for CUDA. For example:
  export PATH=/usr/local/cuda-6.5/bin:$PATH
  export LD_LIBRARY_PATH=/usr/local/cuda-6.5/lib64:$LD_LIBRARY_PATH
* run: scala test